# NextcloudServerConfig

NextcloudServerConfig is a [Nextcloud](https://nextcloud.com/) server configuration tutorial for Debian 12 GNU/Linux (and derivates), its based on Nextclouds [documentation](https://docs.nextcloud.com/server/latest/admin_manual/index.html) and tested by me and friends for daily use on Debian GNU/Linux, Ubuntu and Linux Mint.

This Server configuration uses:

* Apache 2.4
* MariaDB 10.6 to 11.4
* PHP 8.2
* APCu Data Cache
* Redis Memory Cache and Transactional File Locking

## Hardware Architektures

* armhf (not recommended)
* arm64
* amd64

You should set your configuration so that your server have enough resources to serve Nextcloud!
My recommendaded minimum RAM size for a Debian Nextcloud Server is 2GB, but 1GB can be enough for 1 user too (dietPi can help a little ;P)

## Installation Tutorials

If you want Certbot that helps you get started with Let's Encrypt certificates look here:
* [Let's Encrypt](https://gitlab.com/tiagotome95/NextcloudServerConfig/-/tree/master/letsencrypt)

If you just want to use your server at Home or have to use a Self Signed SSL Certificate for some reason:
* [Self Signed SSL Certificate](https://gitlab.com/tiagotome95/NextcloudServerConfig/-/tree/master/selfsigned)

## Backup & Restore Tutorials

* [Backup to local media](https://gitlab.com/tiagotome95/NextcloudServerConfig/-/tree/master/backupToDisk)

* [Backup Remotely](https://gitlab.com/tiagotome95/NextcloudServerConfig/-/tree/master/backupRemote)

## Sources and Links

Nextcloud Docs - [Admin Manual](https://docs.nextcloud.com/server/latest/admin_manual/index.html)

Nextcloud [Installation on Linux](https://docs.nextcloud.com/server/latest/admin_manual/installation/source_installation.html)

Here you can find the last version of [Nextcloud Server and Client Documentation](https://docs.nextcloud.com/) too.


## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment

Tiago Simões Tomé <tiagotome95@gmail.com>
Thanks to all contributors working on NextCloud, ownCloud, and all other free (and open) software projects like Debian that lets you start in your favorite architecture :D

## License
The documentation in this project is licensed under the [Creative Commons Attribution-ShareAlike 4.0 license](https://choosealicense.com/licenses/cc-by-sa-4.0/), the source code content (also the source code included in the documentation) is licensed under the [GNU GPLv3 license](https://choosealicense.com/licenses/gpl-3.0/).
