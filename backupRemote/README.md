# NextcloudServerBackup

Backup your Nextcloud Server instance remotely.

## Introduction

Have a backup plan is one of the most important things when you serve Data or Services for you or other people. When a Solid Disk gets old, the server don't work correctly anymore after misconfiguration or anything else, it is important to have a backup from them you can restore your Nextcloud instance with all the:

* configuration
* data
* themes
* database

We would do the backup on a Client that gets the backup data from the server remotely. We will use ssh, rsync, and scp to get all we need.

Optionaly you can backup the server configuration too, so you don't have to configure it again when you have to restore.

It's recommended to generate authorisation keys to connect to the server securely.

Don't forget to change the username and domainName/ip of your Nextcloud Server and directory names if need, when you type the commands!

## Maintenance mode

Before you begin to upgrade your operating system or nextcloud server or create backups you should put your Nextcloud server to maintenance mode. `maintenance:mode` locks the sessions of logged-in users and prevents new logins in order to prevent inconsistencies of your data. You must run `occ` from the nextcloud directory as the HTTP user, like in this example:

```
ssh root@domain.your "cd /var/www/nextcloud && sudo -u www-data php occ maintenance:mode --on"
```

## Backup folders

If not just done, create the backup folder:

```
mkdir nextcloud_backup
```

and use for example `rsync` to copy the `nextcloud` folder:
```
sudo rsync --progress -Aavx -e 'ssh -i .ssh/id_ed25519' root@domain.your:/var/www/nextcloud/ ~/nextcloud_backup/nextcloud-dirbkp_`date +"%Y%m%d"`/
```

if you don't have authorisation keys:
```
sudo rsync --progress -Aavx root@domain.your:/var/www/nextcloud/ ~/nextcloud_backup/nextcloud-dirbkp_`date +"%Y%m%d"`/
```

and now backup the user Data, this can take a while:
```
sudo rsync --progress -Aavx -e 'ssh -i .ssh/id_ed25519' root@domain.your:/var/nextcloud_data/ ~/nextcloud_backup/nextcloud_data-dirbkp_`date +"%Y%m%d"`/
```

if you don't have authorisation keys:
```
sudo rsync --progress -Aavx root@domain.your:/var/nextcloud_data/ ~/nextcloud_backup/nextcloud_data-dirbkp_`date +"%Y%m%d"`/
```

## Backup database (MySQL/MariaDB)

MySQL or MariaDB, which is a drop-in MySQL replacement, is the recommended database engine. To backup MySQL/MariaDB:

```
ssh root@domain.your "mysqldump --single-transaction -h localhost -u tiago -p nextcloud_db > ~/nextcloud-sqlbkp_`date +"%Y%m%d"`.sql"
```
then type the database user password.

If you use enabled MySQL/MariaDB 4-byte support (Enabling MySQL 4-byte support, needed for emoji), you will need to add --default-character-set=utf8mb4 like this:

```
ssh root@domain.your "mysqldump --single-transaction --default-character-set=utf8mb4 -h localhost -u dbuser -p nextcloud_db > ~/nextcloud-sqlbkp_`date +"%Y%m%d"`.sql"
```
and type the database user password.

Copy database backup file:

```
scp root@domain.your:~/nextcloud-sqlbkp_* ~/nextcloud_backup
```

Remove database backup files on remote server:
```
ssh root@domain.your "rm -v ~/nextcloud-sqlbkp*"
```

## Disable Maintenance mode

Don't forget to put `maintenance:mode` to `off` so all users can use Nextcloud again:

```
ssh root@domain.your "cd /var/www/nextcloud && sudo -u www-data php occ maintenance:mode --off"
```

## Sources and Links

Nextcloud Docs - [Backup](https://docs.nextcloud.com/server/latest/admin_manual/maintenance/backup.html#backup-database)


## Authors and acknowledgment

Tiago Simões Tomé <tiagotome95@gmail.com>

## License
The documentation in this project is licensed under the [Creative Commons Attribution-ShareAlike 4.0 license](https://choosealicense.com/licenses/cc-by-sa-4.0/), the source code content (also the source code included in the documentation) is licensed under the [GNU GPLv3 license](https://choosealicense.com/licenses/gpl-3.0/).
