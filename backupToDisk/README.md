# NextcloudServerBackup

Backup your Nextcloud Server to a (local) Disk or removable media like a Flash Drive.

## Introduction

Have a backup plan is one of the most important things when you serve Data or Services for you or other people. When a Solid Disk gets old, the server don't work correctly anymore after misconfiguration or anything else, it is important to have a backup from them you can restore your Nextcloud instance with all the:

* configuration
* data
* themes
* database

## Maintenance mode

Before you begin to upgrade your operating system or nextcloud server or create backups you should put your Nextcloud server to maintenance mode. `maintenance:mode` locks the sessions of logged-in users and prevents new logins in order to prevent inconsistencies of your data. You must run `occ` from the nextcloud directory as the HTTP user, like in this example:

```
cd /var/www/nextcloud/
```

put server to maintenance mode
```
sudo -u www-data php occ maintenance:mode --on
```
### Alternative way to enable maintenance mode
You may also put your server into this mode by editing `config/config.php`. Change `"maintenance" => false` to `"maintenance" => true`:

```
<?php

 "maintenance" => true,
```

Don’t forget to change it back to `false` when you are finished.

## Backup folders

To backup the data and configuration of your Nextcloud instance, simply copy your whole Nextcloud install and data folder to a place outside of your Nextcloud environment. To do this you can change back to `/var/www` directory:

```
cd /var/www
```

and use for example `rsync` to copy the `nextcloud` folder:

```
rsync -Aavx nextcloud/ /media/USER/nextcloudBackup/nextcloud-dirbkp_`date +"%Y%m%d"`/
```

then change to the folder where the user Data of your nextcloud instance is situated, if your data folder path is `/var/nextcloud_data` change to `/var` directory:

```
cd /var
```
and now backup the user Data, this can take a while:
```
rsync -Aavx nextcloud_data/ /media/USER/nextcloudBackup/nextcloud_data-dirbkp_`date +"%Y%m%d"`/
```

## Backup database (MySQL/MariaDB)

MySQL or MariaDB, which is a drop-in MySQL replacement, is the recommended database engine. To backup MySQL/MariaDB:

```
mysqldump --single-transaction -h [server] -u [username] -p [db_name] > /media/USER/nextcloudBackup/nextcloud-sqlbkp_`date +"%Y%m%d"`.bak
```

If you use enabled MySQL/MariaDB 4-byte support (Enabling MySQL 4-byte support, needed for emoji), you will need to add --default-character-set=utf8mb4 like this:

```
mysqldump --single-transaction --default-character-set=utf8mb4 -h [server] -u [username] -p [db_name] > /media/USER/nextcloudBackup/nextcloud-sqlbkp_`date +"%Y%m%d"`.bak
```

## Disable Maintenance mode

Don't vorget to put `maintenance:mode` to off so all users can use Nextcloud again:

```
cd /var/www/nextcloud/
```

put maintenance mode off:

```
sudo -u www-data php occ maintenance:mode --off
```

## Sources and Links

Nextcloud Docs - [Backup](https://docs.nextcloud.com/server/latest/admin_manual/maintenance/backup.html#backup-database)


## Authors and acknowledgment

Tiago Simões Tomé <tiagotome95@gmail.com>

## License
The documentation in this project is licensed under the [Creative Commons Attribution-ShareAlike 4.0 license](https://choosealicense.com/licenses/cc-by-sa-4.0/), the source code content (also the source code included in the documentation) is licensed under the [GNU GPLv3 license](https://choosealicense.com/licenses/gpl-3.0/).
