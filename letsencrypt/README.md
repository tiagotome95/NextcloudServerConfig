# Nextcloud installation on Debian 12 bookworm

Server configuration uses:

* Apache 2.4
* MariaDB 10.6 to 11.4
* PHP 8.2
* APCu Data Cache
* Redis Memory Cache and Transactional File Locking
* Let's Encrypt SSL/TLS Certificates

## Install (LAMP-Server) Apache, MariaDB, PHP & dependencies

Install required packets
```
sudo apt install php8.2 apache2 libapache2-mod-php8.2 mariadb-server php8.2-xml php8.2-cli php8.2-cgi php8.2-mysql php8.2-mbstring php8.2-gd php8.2-curl php8.2-zip php8.2-bz2 php8.2-intl php8.2-gmp php8.2-bcmath php8.2-imagick imagemagick php8.2-fpm wget curl unzip -y
```
Now we'll encrase the resources by modifying PHP configuration file (`php.ini`) :
```
sudo nano /etc/php/8.2/apache2/php.ini
```
(Note: with the `nano` Text editor you can search with `CTRL`+`W` then type anything like "memory" and `ENTER`, you can `CTRL`+`W` and `ENTER` to show the next result)
```
memory_limit = 6G            # min. 512M
upload_max_filesize = 10G
post_max_size = 11G          # post_max_size should be > upload_max_filesize
max_execution_time = 3600
date.timezone = Europe/Berlin
;output_buffering            # Comment all output_buffering with an ";" (there are more then one)
opcache.enable = 1
opcache.enable_cli = 1
opcache.memory_consumption = 128
opcache.interned_strings_buffer = 16
opcache.max_accelerated_files = 10000
opcache.save_comments = 1
opcache.revalidate_freq = 60
opcache.jit = 1255
opcache.jit_buffer_size = 128M
```

you can set `opcache.validate_timestamps = 0` in `sudo nano /etc/php/8.2/apache2/php.ini` for better performance but then you have to restart PHP after every change to `config.php`.

For more information about the configuration read the [Docs](https://www.php.net/manual/en/ini.core.php). ;P

Start and enable Apache2 Server and MariaDB:

```
sudo systemctl start apache2
sudo systemctl start mariadb
sudo systemctl enable apache2
sudo systemctl enable mariadb
```

## Create and Configure Database for Nextcloud

```
sudo mysql_secure_installation
```

```
sudo mysql -u root -p
```

set username and password for the database:
```
MariaDB [(none)]> CREATE USER 'tiago'@'localhost' IDENTIFIED BY 'password';
MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'tiago'@'localhost';

MariaDB [(none)]> FLUSH PRIVILEGES;
MariaDB [(none)]> EXIT;
```

## Download Nextcloud

```
wget https://download.nextcloud.com/server/releases/latest.zip
unzip latest.zip
```

```
sudo mv nextcloud /var/www/
```

```
sudo chown -R www-data:www-data /var/www/nextcloud/
sudo chmod -R 755 /var/www/nextcloud/
sudo rm -rf /var/www/html
```

```
sudo mkdir /var/nextcloud_data        # Private data
sudo chown -R www-data:www-data /var/nextcloud_data/
sudo chmod -R 755 /var/nextcloud_data/
```

## Configure Apache for Nextcloud

```
sudo nano /etc/apache2/sites-available/nextcloud.conf
```

set your domain name or subdomain:
```
<VirtualHost *:80>
    ServerAdmin tiago@localhost
    DocumentRoot /var/www/nextcloud/
    ServerName http://domain.your/

    Alias / "/var/www/nextcloud/"

    <Directory /var/www/nextcloud/>
        Options FollowSymlinks MultiViews
        AllowOverride All
        Require all granted
        <IfModule mod_dav.c>
            Dav off
        </IfModule>
        SetEnv HOME /var/www/nextcloud
        SetEnv HTTP_HOME /var/www/nextcloud
    </Directory>

    <IfModule mod_headers.c>
        Header always set Strict-Transport-Security "max-age=15768000; preload"
    </IfModule>

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>
```

Load nextcloud.conf to apache2 and activate required modules:

```
sudo a2ensite nextcloud.conf
sudo a2enmod rewrite
sudo a2enmod headers
sudo a2enmod env
sudo a2enmod dir
sudo a2enmod mime
```

Reload and Restart apache2:

```
sudo systemctl reload apache2
sudo systemctl restart apache2
```

# Install Certbot to create and manage Let's Encrypt SSL/TLS Certificates and configure Firewall

enable apache SSL mod:

```
sudo a2enmod ssl
```

Install Certbot and iptables (Firewall):

```
sudo apt install software-properties-common python3-certbot-apache iptables
```

Configure Firewall:

```
sudo iptables -I INPUT -p tcp --dport 80 -j ACCEPT
sudo iptables -I INPUT -p tcp --dport 443 -j ACCEPT
```

Generate Let's encrypt certificates witch certbort:

```
sudo certbot --apache
```
give your own Variables like in the example:

```
Name: your Full Name
Email Address: your@mail.org
```
and select your Nextcloud-configuration:

```
nextcloud.conf
```

## Modify the Unencrypted Virtual Host File to Redirect allways to HTTPS

```
sudo nano /etc/apache2/sites-available/000-default.conf
```

Set Redirect line to force https:

```
<VirtualHost *:80>

        Redirect "/" "https://domain.your/"

</VirtualHost>
```

## Enable Changes in Apache

```
sudo a2enmod ssl
sudo a2enmod headers
sudo a2ensite nextcloud-le-ssl.conf
```

```
sudo systemctl reload apache2
```

```
sudo apache2ctl configtest
```

```
sudo systemctl restart apache2
```

## open the browser and go to ur domain (https://domain.your)

give the right data path:

```
/var/nextcloud_data/
```

## Make a cronjob and other things for DB to work right

```
sudo crontab -u www-data -e
```

```
*/5 * * * * php -f /var/www/nextcloud/cron.php
```

chmod nextcloud occ to execute commands:

```
sudo chmod +x /var/www/nextcloud/occ
```

change to your nextcloud directory and make last db works:

```
cd /var/www/nextcloud/
```

```
sudo -u www-data php occ db:convert-filecache-bigint
sudo -u www-data php occ db:add-missing-indices
sudo -u www-data php occ db:add-missing-primary-keys
```

### (OPTIONAL) Check Nextcloud_data filesystem for changes
You can let Nextcloud check for files that was put in Nextcloud_data filesystem manually or by another service like NFS, SFTP, SMB, etc.

Edit config.php:

```
sudo nano /var/www/nextcloud/config/config.php
```

add this line:

```
'filesystem_check_changes' => 1,
```

## Memory caching with Redis + transitional file locking

install redis-server and php-redis module:

```
sudo apt install redis-server php8.2-redis php8.2-apcu -y
```

ps ax to verify if Redis daemon is running:

```
ps ax | grep redis
```

that shows everything like:

```
2763 ? Ssl 0:00 /usr/bin/redis-server 127.0.0.1:6379
```

config config.php:

```
sudo nano /var/www/nextcloud/config/config.php
```

add this lines and set password:
```
  'memcache.local' => '\OC\Memcache\APCu',
  'memcache.distributed' => '\OC\Memcache\Redis',
  'memcache.locking' => '\OC\Memcache\Redis',
  'redis' => [
      'host'     => '/var/run/redis/redis-server.sock',
      'port'     => 0,
      'dbindex'  => 0,
      'password' => 'foobar',
      'timeout'  => 1.5,
  ],
```

config redis.conf:

```
sudo nano /etc/redis/redis.conf
```

set or uncomment:

```
daemonize no
supervised systemd

port 0
tcp-backlog 511

pidfile /var/run/redis/redis-server.pid
unixsocket /var/run/redis/redis-server.sock

timeout 0

requirepass foobar
```

config redis server in /etc/php/8.2/apache2/php.ini:
```
sudo nano /etc/php/8.2/apache2/php.ini
```

add this lines to the end of the file:
```
[redis]
redis.session.locking_enabled=1
redis.session.lock_retries=-1
redis.session.lock_wait_time=10000
```

config APCu-memcache in /etc/php/8.2/cli/php.ini:
```
sudo nano /etc/php/8.2/cli/php.ini
```

add this line to the end of the file:
```
[apcu]
apc.enable_cli=1
```

Add webserver User to the redis Group:

```
sudo usermod -a -G redis www-data
```

Restart Web server to take effect:

```
sudo systemctl restart apache2
```

### if you have problems with Nextcloud after install Redis-server

Look for service status and logs with:
```
sudo systemctl status redis.service
sudo journalctl -xe
```

```
sudo cat /var/log/redis/redis-server.log
```

probably you have to:
* set virtual memory overcommit to value 1, that means that the kernel pretends there is always enough memory until it actually runs out.

```
sudo nano /etc/sysctl.conf
```
```
vm.overcommit_memory = 1
```
and then reboot to take effect, or run the command:
```
sudo sysctl vm.overcommit_memory=1
```

and

* if you have Transparent Huge Pages (THP) support enabled in your kernel, this will create latency and memory usage issues with Redis.
To fix this issue run the command:
```
sudo nano /etc/default/grub
```
and add transparent_hugepage=never or madvise:
```
GRUB_CMDLINE_LINUX="transparent_hugepage=madvise"
```
update grub:
```
sudo update-grub
```
to show if it takes effect:
```
sudo cat /sys/kernel/mm/transparent_hugepage/enabled
cat /proc/meminfo

```


## php-fpm

Configure `/etc/php/8.2/fpm/pool.d/www.conf`
```
env[HOSTNAME] = $HOSTNAME
env[PATH] = /usr/local/bin:/usr/bin:/bin
env[TMP] = /tmp
env[TMPDIR] = /tmp
env[TEMP] = /tmp
pm.max_children = 34
pm.start_servers = 16
pm.min_spare_servers = 11
pm.max_spare_servers = 22
pm.max_requests = 2000
```

configure `/etc/php/8.2/fpm/php.ini`
```
memory_limit = 6G
;output_buffering = 
max_execution_time = 3600
max_input_time = 3600
post_max_size = 11G
upload_max_filesize = 10G
session.cookie_secure = True
opcache.enable=1
opcache.enable_cli=1
opcache.memory_consumption=128
opcache.interned_strings_buffer=8
opcache.max_accelerated_files=10000
opcache.revalidate_freq=60
opcache.save_comments=1
allow_url_fopen =.*/allow_url_fopen = 1
```

enable fpm
```
a2enconf php8.2-fpm
a2enmod proxy_fcgi setenvif
```


## add lines on .htaccess for well-known/carddav and caldav

```
sudo nano /var/www/nextcloud/.htaccess
```

```
<IfModule mod_rewrite.c>
  RewriteEngine on
  RewriteRule ^\.well-known/carddav /remote.php/dav [R=301,L]
  RewriteRule ^\.well-known/caldav /remote.php/dav [R=301,L]
  RewriteRule ^\.well-known/webfinger /index.php/.well-known/webfinger [R=301,L]
  RewriteRule ^\.well-known/nodeinfo /index.php/.well-known/nodeinfo [R=301,L]
</IfModule>
```

make changes on sites-available/default-ssl.conf:

```
sudo nano /etc/apache2/sites-available/default-ssl.conf
```

add this:

```
                DocumentRoot /var/www/nextcloud

                <Directory /var/www/nextcloud>
                        AllowOverride All
                </Directory>
```

Restart apache2:
```
sudo systemctl restart apache2
```

# Wartung

if write permission dont let nextcloud upgrade:
```
sudo chown -R www-data:www-data /var/www/nextcloud
```

change to your nextcloud directory:
```
cd /var/www/nextcloud/
```

Change phone region to your local region
```
sudo -u www-data php occ config:system:set default_phone_region --value=de
```

Startzeit für das Wartungsfenster (ab 1h)
```
sudo -u www-data php occ config:system:set maintenance_window_start --type=integer --value=1
```


**scan:**

```
sudo -u www-data php occ files:scan --all
```

**add missing indices:**

```
sudo -u www-data php occ db:add-missing-indices
```

**cleanup:**

```
sudo -u www-data php occ files:cleanup
```

**trashbin-claenup:**

```
sudo -u www-data php occ trashbin:cleanup --all-users
```
